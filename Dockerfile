FROM jenkins/jenkins:lts

USER root

# Get php dependencies
RUN apt update && \
	apt install -y ca-certificates apt-transport-https wget curl unzip git lsb-core zip && \
    wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add - && \
    echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list && \
    apt update && \
    apt install -y php7.2 php7.2-bcmath php7.2-xml php7.2-json php7.2-mbstring php7.2-zip && \
	rm -rf /var/lib/apt/lists/*

# Install composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer